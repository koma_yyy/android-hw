package pw.komarov.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.Format;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        ((TextView)findViewById(R.id.textView)).
                setText(getString(R.string.hello_with_name,
                        ((TextView)findViewById(R.id.editText)).getText()));
    }
}